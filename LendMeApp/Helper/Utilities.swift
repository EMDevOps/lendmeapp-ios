//
//  Utilities.swift
//  LendMeApp
//
//  Created by Avinas Udayakumar - EMedia on 10/10/18.
//  Copyright © 2018 Avinas Udayakumar. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift
import ObjectMapper
import PromiseKit
import UIKit

class Utility {
    
    //global variables usin sigleton
    static let shared = Utility()
    
    //MARK:Variables
    var errorCode: Int = 0
    
    
    
    //Errro code messages function
    func messageForErrorCode(errorCode: Int) -> String {
        var message: String = ""
        switch errorCode {
        case 400:
            message = NSLocalizedString("BadRequest", comment: "")
        case 401:
            message = NSLocalizedString("authentication_error", comment: "")
        case 403:
            message = NSLocalizedString("no_permission", comment: "")
        case 404:
            message = NSLocalizedString("data_not_found", comment: "")
        case 422:
            message = NSLocalizedString("data_not_in_correct_format", comment: "")
        case 500...599:
            message = NSLocalizedString("server_error", comment: "")
        case 600:
            message = NSLocalizedString("data_outdated", comment: "")
        case 1009:
            message = NSLocalizedString("internet_error", comment: "")
        default:
            message = NSLocalizedString("unknown_error", comment: "")
        }
        return message
    }
    
    //to get device id
    static var deviceId: String {
        return UIDevice.current.identifierForVendor!.uuidString
    }
    
    
}
