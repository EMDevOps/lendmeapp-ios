//
//  Configuration.swift
//  LendMeApp
//
//  Created by Avinas Udayakumar - EMedia on 10/10/18.
//  Copyright © 2018 Avinas Udayakumar. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyBeaver

//completion handler
typealias completionHandler = (_ success: Bool, _ message: String?) -> Void

//
let log = SwiftyBeaver.self

class Configuration {
    
    static let shared = Configuration()
    
    func loggerSetup() {
        // add log destinations. at least one is needed!
        let console = ConsoleDestination()  // log to Xcode Console
        let file = FileDestination()  // log to default swiftybeaver.log file
        // add the destinations to SwiftyBeaver
        log.addDestination(console)
        log.addDestination(file)
    }
    
}


//common class for realm
final class RealmConfiguration {
    class func realm() -> Realm {
        return try! Realm()
    }
}
