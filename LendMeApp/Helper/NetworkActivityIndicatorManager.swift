//
//  NetworkActivityIndicatorManager.swift
//  LendMeApp
//
//  Created by Avinas Udayakumar - EMedia on 10/10/18.
//  Copyright © 2018 Avinas Udayakumar. All rights reserved.
//

import Foundation
import UIKit
import Moya


struct NetworkActivityIndicatorManager {
    
    let closure = NetworkActivityPlugin { (change, target) in
        switch change {
        case .began :  UIApplication.shared.isNetworkActivityIndicatorVisible = true
        case .ended :  UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
    }
    
}

