//
//  Constant.swift
//  LendMeApp
//
//  Created by Avinas Udayakumar - EMedia on 10/10/18.
//  Copyright © 2018 Avinas Udayakumar. All rights reserved.
//

import Foundation

struct Constant {
    
    struct ThemeColors {
        static let navigationBarTintColor = "#02102F"
        static let navigationTintColor = "#FFFFFF"
        static let navigationbarFontColor = "#FFFFFF"
        static let placeHolderStartColor = "#FD1A5F"
    }
    
    struct ThemeFonts {
        static let SFProTextRegular = "SFProText-Regular"
        static let SFProTextBold = "SFProText-Bold"
    }
    
    struct ViewControllersIdentifier {
        static let popUpMenuVC = "MenuPopVC"
    }
    
    struct AppObjects {
        static let appDelegate = UIApplication.shared.delegate as! AppDelegate
    }
    
    struct StoryBoardIdentifiers {
        static let Main = "Main"
    }
    
    struct RemeberMe {
        static let remembered = "REMEMBERED"
        static let notRemembered = "NOT_REMEMBERED"
    }
    
}
