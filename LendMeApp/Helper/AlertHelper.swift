//
//  AlertHelper.swift
//  LendMeApp
//
//  Created by Avinas Udayakumar - EMedia on 10/10/18.
//  Copyright © 2018 Avinas Udayakumar. All rights reserved.
//

import Foundation
import UIKit

class AlertHelper {
    
    //defining a variable for VC
    var viewController: UIViewController
    
    //initilizing the viewControllers
    init(controller: UIViewController) {
        self.viewController = controller
    }
    
    //simple OK alert
    func showOkAlert(title: String, message: String, okTitle:String) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        alertController.addAction(UIAlertAction(title: okTitle, style: .default, handler: nil))
        
        viewController.present(alertController, animated: true, completion: nil)
    }
    
    //Simple Ok Alert with action
    func showAlertWithAction(title: String, message: String, okTitle:String, completion: @escaping (_ action: UIAlertAction) -> ()) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: okTitle, style: .default) { (action: UIAlertAction) in
            completion(action)
        }
        
        alertController.addAction(okAction)
        viewController.present(alertController, animated: true, completion: nil)
    }
    
    //Confirmation Alert with yes no
    func showConfirmationAlert(title: String, message: String, cancelTitle: String, okTitle: String, completion: @escaping (_ action: UIAlertAction) -> ()) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        let cancelAction = UIAlertAction(title: cancelTitle, style: .cancel) { (action: UIAlertAction) in
            completion(action)
        }
        
        let okAction = UIAlertAction(title: okTitle, style: .default) { (action: UIAlertAction) in
            completion(action)
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(okAction)
        viewController.present(alertController, animated: true, completion: nil)
    }
    
    // Actionsheet with 2 main actions and cancel for Image Picking
    func showActionSheetWithTwoActions(title: String, message: String, cancelTitle: String, actOneTitle: String, actTwoTitle: String, sourceView: UIView , sourceRect: CGRect, completion: @escaping (_ action: UIAlertAction) -> ()) {
        
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        let cancelAction = UIAlertAction(title: cancelTitle, style: .cancel) { (action: UIAlertAction) in
            completion(action)
        }
        
        let actionOne = UIAlertAction(title: actOneTitle, style: .default) { (action: UIAlertAction) in
            completion(action)
        }
        
        let actionTwo = UIAlertAction(title: actTwoTitle, style: .default) { (action: UIAlertAction) in
            completion(action)
        }
        
        alertController.addAction(actionOne)
        alertController.addAction(actionTwo)
        alertController.addAction(cancelAction)
        alertController.popoverPresentationController?.sourceView = sourceView
        alertController.popoverPresentationController?.sourceRect = sourceRect
        viewController.present(alertController, animated: true, completion: nil)
    }
    
}
