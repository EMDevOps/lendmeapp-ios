//
//  NetworkManager.swift
//  LendMeApp
//
//  Created by Avinas Udayakumar - EMedia on 10/10/18.
//  Copyright © 2018 Avinas Udayakumar. All rights reserved.
//

import Foundation
import PromiseKit
import Moya
import SwiftyJSON

//define a enum type for API enviornment
enum APIEnvironment {
    case development
    case distribution
}


//creating struct for networkmanager
struct NetworkManager {
    
    //MARK: Define API key and Header parameters
    
    //API KEY
    private static let LEND_ME_API_KEY = "n4jZGy1kpJx7a4FVfjzTp6CsKdVJsUmXS"
    
    //ENVIORMENT
    static let environment = APIEnvironment.development
    
    //BASE URL
    static var baseUrl: String {
        switch environment {
        case .development : return "http://ato.sandbox6.elegant-media.com/api/v1/"
        case .distribution : return ""
        }
    }
    
    //HEADER PARAMETERS
    static var headerWithAPIKey: [String: String] {
        return ["Content-type": "application/json",
                "Accept": "application/json",
                "Authorization": "Bearer \(LEND_ME_API_KEY)"]
    }
    
    static var headerWithUserToken: [String: String] {
        guard let user = AppUser.current() else { return ["" : ""]}
        
        return ["Content-type": "application/json",
                "Accept": "application/json",
                "Authorization": "Bearer \(user.token)"]
    }
    
    //MARK: Define plugins
    
    //define the network indicator plugin
    private let plugins: [PluginType] = [NetworkLoggerPlugin(verbose: false), NetworkActivityIndicatorManager().closure]
    
    
    //MARK: Define the function for to call API services with promise kit and moya
    func CallAPIService<T: TargetType>(target: T) -> Promise<Moya.Response> {
        //define the assign the plugine for nework indicator
        let provider = MoyaProvider<T>(plugins: plugins)
        
        //peform with return response with promise kit
        return Promise { seal in
            provider.request(target, completion: { (result) in
                //getting the resut using switch case
                switch result {
                case let .success(moyaResponse):
                    seal.fulfill(moyaResponse)
                case let .failure(error):
                    seal.reject(self.handleError(error))
                    log.error("ERROR REASEON:\(error.response?.statusCode ?? 0)")
                }
            })
        }
    }
    
    // MARK: Create Custom error with Error Data
    private func handleError(_ error: MoyaError) -> ResponseError {
        var errorMessage = "Response Error"
        if let errorData = error.response?.data {
            // handle linvalid token
            if let code = error.response?.response?.statusCode, code == 401 {
                // show login page
                AppUser.clearUserSession {
                    Constant.AppObjects.appDelegate.showRoot()
                }
                errorMessage = "Session Expired";
            } else {
                let error = JSON(errorData)
                errorMessage = error["message"].stringValue
            }
        }
        return (ResponseError(errorMessage))
    }
}

//MARK: Struct for error response
struct ResponseError: Error {
    let message: String
    
    init(_ message: String) {
        self.message = message
    }
    
    public var localizedDescription: String {
        return message
    }
}
