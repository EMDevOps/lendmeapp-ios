//
//  AppUser.swift
//  LendMeApp
//
//  Created by Avinas Udayakumar - EMedia on 10/10/18.
//  Copyright © 2018 Avinas Udayakumar. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift
import ObjectMapper

class AppUser: Object, Mappable {
    
    @objc dynamic var userID: Int = 0
    @objc dynamic var fullName: String = ""
    @objc dynamic var lastName: String = ""
    @objc dynamic var email: String = ""
    @objc dynamic var token: String = ""
    @objc dynamic var tokenType: String = ""
    @objc dynamic var expiresIn: Double = 0
    @objc dynamic var isRemembered: String = ""
    @objc dynamic var expireDate: Date?
    @objc dynamic var expireTimestamp: Double = 0.0
    
    
    override class func primaryKey() -> String {
        return "userID"
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    //mapping the json format values into readable values to store in realm path
    func mapping(map: Map) {
        userID <- map["id"]
        fullName <- map["full_name"]
        lastName <- map["last_name"]
        email <- map["email"]
        token <- map["access_token"]
        tokenType <- map["token_type"]
        expiresIn <- map["expires_in"]
    }
    
    //function for to save login details
    class func saveUserLoginSession(json: JSON) {
        //save user data to realm
        guard let user = Mapper<AppUser>().map(JSONObject: json["data"].rawValue) else {
            return
        }
        
        //get the user token and other details
        let token = json["access_token"].stringValue
        let tokentype = json["token_type"].stringValue
        let expiresIn = json["expires_in"].doubleValue
        
        //addign the values to user model
        user.token = token
        user.tokenType = tokentype
        user.expiresIn = expiresIn
        
        let expDate = Date().addingTimeInterval(expiresIn)
        
        user.expireDate = expDate
        user.expireTimestamp = expDate.timeIntervalSince1970
        
        try! RealmConfiguration.realm().write {
            RealmConfiguration.realm().add(user, update: true)
        }
    }
    
    //function for to save login status / remember me status
    class func saveLoginStatus(status: String) {
        try! RealmConfiguration.realm().write {
            AppUser.current()?.isRemembered = status
        }
    }
    
    //funcion for to reterive the user token
    class func current() -> AppUser? {
        let users = RealmConfiguration.realm().objects(AppUser.self)
        return users.first
    }
    
    
    //function for to remove user data once the user logout means
    class func clearUserSession(completion: ()->()) {
        try! RealmConfiguration.realm().write {
            RealmConfiguration.realm().deleteAll()
            completion()
        }
    }
    
    
}
